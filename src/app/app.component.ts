import { Component } from '@angular/core';

import { FrontpageComponent } from './frontpage/frontpage.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'dib-test';
}
