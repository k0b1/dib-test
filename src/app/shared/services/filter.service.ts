import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class FilterService {

  filteredItems: string;
  displayGrid: boolean = true;
  displayList: boolean = false;

  constructor() { }


}
