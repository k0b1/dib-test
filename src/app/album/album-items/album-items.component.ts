import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { ActionsService } from 'src/app/shared/services/actions.service';

@Component({
  selector: 'app-album-items',
  templateUrl: './album-items.component.html',
  styleUrls: ['./album-items.component.scss']
})
export class AlbumItemsComponent implements OnInit {
  @Input() allImages: any[];

  constructor( private actions: ActionsService ) { }

  ngOnInit() {
    this.actions.filteredItems = "";
  }

  onRemove( elem ) {
    // console.log( elem)
    let deleteElement = elem.parentElement;
    deleteElement.style.border = "1px solid red";
  }

}
