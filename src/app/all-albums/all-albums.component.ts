import { Component, OnInit } from '@angular/core';
import { NavigationTopComponent } from '../navigation-top/navigation-top.component';
import { HttpService } from '../shared/services/http.service';
import { ActionsService } from '../shared/services/actions.service';

@Component({
  selector: 'app-album',
  templateUrl: './all-albums.component.html',
  styleUrls: ['./all-albums.component.scss']
})

export class AllAlbumsComponent implements OnInit {
  allAlbums: any[];
  allUsers: any[];

  displayGrid: boolean = true;
  displayList: boolean = false;

  constructor( private getAlbum: HttpService,
               private actions: ActionsService ) { }

  ngOnInit(  ) {
    this.getAlbumImages();
    this.actions.filteredItems = "";
  }

  getAlbumImages() {
    this.getAlbum.getAlbums().subscribe( response => {
      // console.log( response );
      if ( response.length ) {
        this.allAlbums = response;
        this.getAlbum.getUserName().subscribe( response => {
          // console.log( response)
          this.allUsers = response;
          for ( let album in this.allAlbums ) {
            for ( let user in this.allUsers ) {
              if ( this.allAlbums[album].userId === this.allUsers[user].id ) {
                this.allAlbums[album]['username'] = this.allUsers[user].name;
              }
            }
          }
        });
      }
      
    });
  }

  // onDisplayGrid() {
  //   this.displayGrid = true;
  //   this.displayList = false;
  // }

  // onDisplayList() {
  //   this.displayGrid = false;
  //   this.displayList = true;
  // }

  // onRemove( elem ) {
  //   let deleteElement = elem.parentElement;
  //   deleteElement.style.border = "1px solid red";
  // }

}
