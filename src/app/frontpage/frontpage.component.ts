import { Component, OnInit } from '@angular/core';

import { NavigationTopComponent } from "../navigation-top/navigation-top.component";

@Component({
  selector: 'app-frontpage',
  templateUrl: './frontpage.component.html',
  styleUrls: ['./frontpage.component.scss']
})
export class FrontpageComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
