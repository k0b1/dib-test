import { Component, OnInit } from '@angular/core';
import { FilterComponent } from './filter/filter.component';
import { ActivatedRoute } from '@angular/router';
import { ActionsService } from '../../services/actions.service';

@Component({
  selector: 'app-album-actions',
  templateUrl: './album-actions.component.html',
  styleUrls: ['./album-actions.component.scss']
})
export class AlbumActionsComponent implements OnInit {

  hideBackButton: boolean = false;

  constructor( private actions: ActionsService,
               private route: ActivatedRoute ) { }

  ngOnInit() {
    if ( this.route.snapshot.component['name'] == 'AllAlbumsComponent' ) {
      this.hideBackButton = true;
      this.actions.displayGrid = true;
      this.actions.displayList = false;
    }
  }

  onDisplayGrid() {
    this.actions.displayGrid = true;
    this.actions.displayList = false;
  }

  onDisplayList() {
    this.actions.displayGrid = false;
    this.actions.displayList = true;
  }
}
