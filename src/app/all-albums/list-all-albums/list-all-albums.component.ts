import { Component, OnInit, Input } from '@angular/core';
import { ActionsService } from 'src/app/shared/services/actions.service';

@Component({
  selector: 'app-list-all-albums',
  templateUrl: './list-all-albums.component.html',
  styleUrls: ['./list-all-albums.component.scss']
})
export class ListAllAlbumsComponent implements OnInit {
  @Input() allAlbums: any[];
  constructor( private action: ActionsService ) { }

  ngOnInit() {
    this.action.filteredItems = ""; //reset input values
  }

  onRemove( elem ) {
    // console.log( elem)
    let deleteElement = elem.parentElement;
    deleteElement.style.border = "1px solid red";
  }

}
