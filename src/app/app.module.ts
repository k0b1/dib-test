import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { FrontpageComponent } from './frontpage/frontpage.component';
import { NavigationTopComponent } from './navigation-top/navigation-top.component';
import { AllAlbumsComponent } from './all-albums/all-albums.component';
import { AlbumComponent } from './album/album.component';
import { FilterPipe } from './shared/pipes/filter.pipe';
import { AlbumActionsComponent } from './shared/components/album-actions/album-actions.component';
import { FilterComponent } from './shared/components/album-actions/filter/filter.component';
import { AlbumItemsComponent } from './album/album-items/album-items.component';
import { ListAllAlbumsComponent } from './all-albums/list-all-albums/list-all-albums.component';
@NgModule({
  declarations: [
    AppComponent,
    FrontpageComponent,
    NavigationTopComponent,
    AllAlbumsComponent,
    AlbumComponent,
    FilterPipe,
    AlbumActionsComponent,
    FilterComponent,
    AlbumItemsComponent,
    ListAllAlbumsComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
