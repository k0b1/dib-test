import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlbumActionsComponent } from './album-actions.component';

describe('AlbumActionsComponent', () => {
  let component: AlbumActionsComponent;
  let fixture: ComponentFixture<AlbumActionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlbumActionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlbumActionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
