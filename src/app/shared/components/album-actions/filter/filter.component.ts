import { Component, OnInit } from '@angular/core';
import { FilterService } from 'src/app/shared/services/filter.service';
import { ActionsService } from 'src/app/shared/services/actions.service';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss']
})
export class FilterComponent implements OnInit {
  filteredImages: string;

  constructor( private filterService: ActionsService) { }

  ngOnInit() {
    console.log( this.filteredImages )
  }

  onFilterChange(event) {
    this.filterService.filteredItems = event;
    console.log( this.filterService.filteredItems );
  }


}
