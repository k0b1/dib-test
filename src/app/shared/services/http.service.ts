import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class HttpService {
  baseUrl = "https://jsonplaceholder.typicode.com";
  urlAlbums = this.baseUrl + "/albums";
  urlAlbumSingle = this.baseUrl + "/photos";
  urlUsers = this.baseUrl + "/users";

  constructor( private httpClient: HttpClient) { }

  getAlbums() {
    return this.httpClient.get<any[]>(this.urlAlbums );
  }

  getAlbumImages( albumID ) {
    return this.httpClient.get<any[]>( this.urlAlbumSingle+"?albumId="+albumID );
  }

  getUserName() {
    return this.httpClient.get<any[]>( this.urlUsers );
  }

}
