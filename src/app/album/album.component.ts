import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpService } from '../shared/services/http.service';

import { AlbumActionsComponent } from '../shared/components/album-actions/album-actions.component';
import { AlbumItemsComponent } from '../album/album-items/album-items.component';
import { ActionsService } from '../shared/services/actions.service';

@Component({
  selector: 'app-album',
  templateUrl: './album.component.html',
  styleUrls: ['./album.component.scss']
})
export class AlbumComponent implements OnInit {
  
  albumID: number;
  allImages: any[];

  constructor( 
    private route: ActivatedRoute,
    private getImages: HttpService,
    private filterService: ActionsService
  ) { }

  ngOnInit() {
    this.albumID = this.route.snapshot.params['albumID'];
    this.getAlbumImages(); 
  }


  getAlbumImages() {
    this.getImages.getAlbumImages( this.albumID ).subscribe( response => {
      this.allImages = response;
    });
  }
}
