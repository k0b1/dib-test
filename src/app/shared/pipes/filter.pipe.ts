import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(value: any, filterImages: string, propName: string): any {
    if ( !value || !filterImages ) {
      return value;
    }
    const resultArray = [];
    for ( const item of value ) {
    //   if ( item[propName] === filterImages ) {
    //     resultArray.push(item);
    if ( item[propName].startsWith(filterImages) ) {
      resultArray.push( item );
    }
      }
      
    
    return resultArray;
  }

}
