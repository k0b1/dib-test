import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListAllAlbumsComponent } from './list-all-albums.component';

describe('ListAllAlbumsComponent', () => {
  let component: ListAllAlbumsComponent;
  let fixture: ComponentFixture<ListAllAlbumsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListAllAlbumsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListAllAlbumsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
